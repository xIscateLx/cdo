# Центр делового образования

Цель проекта - написать небольшой сайт для одного учебного заведения города Омска. Заказчику требовался небольшой сингл педж, на котором они смогут указывать список учителей, программ обучения и отзывов, с минимальной панелью администратора.
Общая продолжительность проекта меньше недели, 20-30 часов.


## Используемые технологии

Backend: Laravel 5.4, MySQL
***
Frontend: Jquery, VueJS.

## Development environment with Docker
1. Install Docker + Docker Compose (https://docs.docker.com/compose)

2. Get project:

```
$ git clone <url-to-repo>
```
3. Run the docker containers:

```
$ cd /path/to/project
$ docker-compose --file docker-compose.yml up -d
```
5. Go to `http://127.0.0.1:8000` and enjoy