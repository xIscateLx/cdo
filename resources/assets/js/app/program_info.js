/**
 * Created by Рост on 15.01.2017.
 */

$(document).ready(function () {
    $('.program-icons .item').on('click', function (event) {
        event.preventDefault();
        var status = false;
        if ($(this).hasClass('active')) {
            status = true;
        }
        var toggle = $(this).data('toggle');

        $('.program-icons').find('.active').removeClass('active');
        $('.program-list').find('.program-info.active').removeClass('active').fadeOut(500);

        if (!status) {
            $(this).addClass('active');
            $('.program-list').find(toggle).addClass('active').fadeIn(500);

            // if (window.innerWidth > 768) {
            //     return;
            // }
            var $navPanel = $('.top-main-navbar');
            var offset = $navPanel.height();

            var $target = $('.program-list').find(toggle);
            if ($target.length > 0) {
                $('html, body').animate({
                        scrollTop: $target.position().top - offset
                    },
                    500
                );
            }

        }
    });

    $('.program-info .close').click(function (event) {
        event.preventDefault();
        var activeIcons = $('.program-icons').find('.active').first();
        $('.program-icons').find('.active').removeClass('active');
        $('.program-list').find('.program-info.active').removeClass('active').fadeOut(500);

        // if (window.innerWidth > 768) {
        //     return;
        // }

        var $navPanel = $('.top-main-navbar');
        var offset = $navPanel.height();
        if ($(window).width() < 768) {
            activeIcons = $('.program');
        }
        $('html, body').animate({
                scrollTop: activeIcons.position().top - offset
            },
            500
        );
    });

    var slickSettings = {
        arrows: false,
        // the magic
        responsive: [{
            breakpoint: 10000,
            settings: "unslick" // destroys slick
        }, {
            breakpoint: 768,
            settings: {
                dots: true,
                infinite: true,
                speed: 300,
                autoplay: false,
                slidesToShow: 1
            }
        }]
    };

    $('.program-icons').slick(slickSettings);

    $(window).resize(function () {
        if ($('.program-icons').slick('getSlick').unslicked && $(window).width() < 768) {
            $('.program-icons').slick(slickSettings);
        }
    })
});