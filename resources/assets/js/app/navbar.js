/**
 * Created by rostislav on 30.01.17.
 */

$(document.body).ready(function () {
    $('.navbar-toggle').on('click', function (event) {
        event.preventDefault();
        toggleNavbar($(this));
    });

    $(document).mouseup(function (e) {
        var div = $(".top-main-navbar");
        var toggle = div.find('.navbar-toggle');
        if (toggle.hasClass('active') && !div.is(e.target) && div.has(e.target).length === 0) {
            toggleNavbar(toggle);
        }
    });

    function toggleNavbar(div) {
        var toggle = div.hasClass('active');
        var navbar = $(div.data('target'));

        if (toggle) {
            div.removeClass('active');
            navbar.removeClass('active');
        } else {
            div.addClass('active');
            navbar.addClass('active');
        }
    }
});
