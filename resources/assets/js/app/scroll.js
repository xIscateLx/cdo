$(document).ready(function() {
    $('.navbar-nav a').click(function(event) {
        event.preventDefault();

        var $navPanel = $('.top-main-navbar');
        var offset = $navPanel.height();

        if (!$navPanel.hasClass('full-top')) {
            offset += $navPanel.offset().top;
        }

        if (screen.width < 767) {
            offset = 0;
        }

        var target = $(this).attr('href');

        $('html, body').animate({
                scrollTop: $(target).position().top - offset},
            500
        );
    })
});
