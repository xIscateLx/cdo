$(document.body).ready(function () {
    var shrinkHeader = 120;
    $(window).scroll(function () {
        var scroll = getCurrentScroll();
        if (scroll >= shrinkHeader) {
            $('body').addClass('no-padding-top');
            $('.header').addClass('hidden-to-top');
            $('.top-main-navbar').addClass('full-top');
        }
        else {
            $('body').removeClass('no-padding-top');
            $('.header').removeClass('hidden-to-top');
            $('.top-main-navbar').removeClass('full-top');
        }
    });
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
});