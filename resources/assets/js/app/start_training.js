/**
 * Created by Рост on 26.12.2016.
 */

$(document.body).ready(function () {
    $('#send-student-request').click(function (event) {
        event.preventDefault();

        var form = $('#student-request-form');
        var data = form.serializeArray();
        var $button = $(this);

        form.find('div.has-error').removeClass('has-error');
        form.find('small.help-block').remove();

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: data,
            success: function (response) {
                $('#contact_form').modal('hide');
                $.gritter.add({
                    text: response.message,
                    class_name: 'color success'
                });
            },
            error: function (data) {
                var errors = data.responseJSON;
                $.each(errors, function (index, value) {
                    var $element = form.find('[name="' + index + '"]');
                    $element.closest('div').addClass('has-error').append(
                        '<small class="help-block">' + value + '</small>');
                });
            },
            beforeSend: function () {
                $button.prop('disabled', true);
            },
            complete: function () {
                $button.prop('disabled', false);
            }
        });
    });

    $('#contact_form').on('hidden.bs.modal', function() {
        var form = $('#student-request-form');
        form[0].reset();
        form.find('div.has-error').removeClass('has-error');
        form.find('small.help-block').remove();
    })

});