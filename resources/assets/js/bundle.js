/**
 * Created by rostislav on 06.02.17.
 */

/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./components/bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('ImageCrop', require('./components/ImageCrop.vue'));

Vue.component('SwitchButton', require('./components/SwitchButton.vue'));

Vue.component('IconChoice', require('./components/IconChoice.vue'));

Vue.component('Program', require('./components/Program.vue'));

Vue.component('ProgramItems', require('./components/ProgramItems.vue'));

import VueTinymce from 'vue-tinymce'
Vue.use(VueTinymce);

Vue.mixin({
    methods: {
        lang: function (value) {
            return Lang.get(value);
        }
    }
});

const app = new Vue({
    el: '#admin'
});