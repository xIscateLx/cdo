/**
 * Created by Рост on 17.10.2016.
 */

$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //initialize the javascript
    App.init({
        openLeftSidebarOnClick: true
    });

    if ($('[data-toggle=confirmation]').length > 0) {
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            singleton: true,
            onConfirm: removeItem
        });
    }

    $(".select2").select2({
        width: '100%'
    });

    function iconTemplate(state) {
        if (!state.id) {
            return state.next;
        }
        return $('<div class="icons-list"><span class="icon-sm"><svg class="icon icon-sm middle"><use xlink:href="/assets/images/icons.svg' + state.id + '"></use></svg> ' + state.text + '</span></div>');
    };

    $('.select2.with-icon').select2({
        width: '100%',
        templateResult: iconTemplate
    });

    tinymce.init({
        selector: '.tinymce-desc',
        toolbar: 'removeformat | bold italic underline strikethrough | bullist numlist | undo redo outdent indent | alignleft alignright aligncenter alignjustify alignnone',
        plugins: "lists autoresize",
        menubar: false,
        statusbar: false
    });
});

function removeItem() {
    var url = $(this).data('remove');
    var item = $(this).closest('tr');
    $.ajax({
        url: url,
        method: 'DELETE',
        success: function (response) {
            item.remove();
            $.gritter.add({
                text: response.message,
                class_name: 'color success',
            });
            if (response.redirect) {
                window.location.href = response.redirect;
            }
        }
    })
}