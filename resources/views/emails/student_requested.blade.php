@component('mail::message')

@component('mail::panel')

На сайте были поданы следующие данные:

@component('mail::table')
|                           |                               |
| ------------------------  |:-----------------------------:|
| @lang('forms.full_name')  | {{ $request['full_name'] }}   |
| @lang('forms.phone')      | {{ $request['phone'] or '' }} |
| @lang('forms.email')      | {{ $request['email'] }}       |
@endcomponent
@endcomponent

@endcomponent