@extends('auth.layout')

@section('title')
    @lang('auth.forgot_password') | ЦДО
@endsection

@section('content')
    <div class="login-forms">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @else
            <div class="alert alert-warning alert-bold-border fade in alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @lang('passwords.reset_email_notice')
            </div>
        @endif

        <form role="form" method="POST" action="{{ url('/admin/password/email') }}">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" name="email" type="email" class="form-control no-border input-lg rounded"
                       placeholder="@lang('fields.email')" autofocus value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group">
                <button type="submit"
                        class="btn btn-primary btn-lg btn-perspective btn-block">@lang('passwords.reset_email')</button>
            </div>
        </form>
    </div>
@endsection
