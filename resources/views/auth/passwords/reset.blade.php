@extends('auth.layout')

@section('title')
    @lang('auth.change_password') | ЦДО
@endsection

@section('content')
    <div class="login-forms">
        <form role="form" method="POST" action="{{ url('/admin/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group has-feedback lg left-feedback no-label {{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control no-border input-lg rounded"
                       placeholder="@lang('fields.email')" name="email" value="{{ $email or old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback lg left-feedback no-label{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control no-border input-lg rounded"
                       placeholder="@lang('fields.password')" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback lg left-feedback no-label{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="form-control no-border input-lg rounded"
                       placeholder="@lang('fields.re-password')" name="password_confirmation">
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <button type="submit"
                        class="btn btn-warning btn-lg btn-perspective btn-block">@lang('passwords.reset_password')
                </button>
            </div>
        </form>
    </div>
@endsection
