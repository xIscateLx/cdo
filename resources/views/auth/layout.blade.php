<!DOCTYPE html>
<html lang="{{ $app->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CDO">
    <meta name="author" content="Rostislav Lyashev">
    <title>@yield('title')</title>

    @if($app->environment('local'))
        <link href="{{ asset('assets/css/vendor_admin.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    @else
        <link href="{{ elixir('assets/css/vendor_admin.css', '.') }}" rel="stylesheet">
        <link href="{{ elixir('assets/css/admin.css', '.') }}" rel="stylesheet">
    @endif
</head>

@php
    if (empty($variables)) {
        $variables = App\Models\Variable::all()->pluck('value', 'key')->toArray();
    }
@endphp

<body class="am-splash-screen">
<div class="am-wrapper am-login">
    <div class="am-content">
        <div class="main-content">
            <div class="login-container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <img src="{{ (!empty($variables['label']) ? Storage::url($variables['label']) : '' ) }}"
                             class="logo-img" alt="Logo">
                        <span>
                            {!! !empty($variables['site_name']) ? nl2br($variables['site_name']) : '' !!}
                        </span>
                    </div>
                    <div class="panel-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScripts -->
@if($app->environment('local'))
    <script src="{{ asset('assets/js/vendor_admin.js') }}"></script>
    <script src="{{ asset('assets/js/admin.js') }}"></script>
@else
    <script src="{{ elixir('assets/js/vendor_admin.js', '.') }}"></script>
    <script src="{{ elixir('assets/js/admin.js', '.') }}"></script>
@endif

</body>
</html>