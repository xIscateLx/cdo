@extends('auth.layout')

@section('title')
    @lang('auth.login') | ЦДО
@endsection

@section('content')
    <form role="form" method="POST" action="{{ url('/admin/login') }}" class="form-horizontal">
        <div class="login-form">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon s7-user"></i></span>
                    <input id="email" name="email" type="email" class="form-control"
                           placeholder="@lang('fields.email')" autofocus value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                    <input type="password" name="password" class="form-control"
                           placeholder="@lang('fields.password')">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group login-submit">
                <button data-dismiss="modal" type="submit" class="btn btn-primary btn-lg">@lang('auth.login')</button>
            </div>
            <div class="form-group footer row">
                <div class="col-xs-6"><a href="{{ url('admin/password/reset') }}">@lang('auth.forgot_password')</a>
                </div>
                <div class="col-xs-6 remember">
                    <label for="remember">@lang('fields.remember')</label>
                    <div class="am-checkbox">
                        <input type="checkbox" id="remember">
                        <label for="remember"></label>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
