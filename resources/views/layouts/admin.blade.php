@include('js-localization::head')
<!DOCTYPE html>
<html lang="{{ $app->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">


    <title>ЦДО | @yield('title')</title>

    @if($app->environment('local'))
        <link href="{{ asset('assets/css/vendor_admin.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    @else
        <link href="{{ elixir('assets/css/vendor_admin.css', '.') }}" rel="stylesheet">
        <link href="{{ elixir('assets/css/admin.css', '.') }}" rel="stylesheet">
    @endif
</head>
<body>
<div class="am-wrapper" id="admin">

    @include('layouts.partials.nav_default')
    @include('layouts.partials.nav_left')

    <div class="am-content">
        <div class="page-head">
            @section('page_header')
                <h2>
                    @yield('title')
                    <div class="buttons">
                        @yield('action-panel')
                    </div>
                </h2>
                <ol class="breadcrumb">
                    @yield('breadcrump')
                </ol>
            @show
        </div>

        <div class="main-content">
            <div class="row">
                @if(session('message'))
                    <div role="alert" class="alert alert-success alert-dismissible">
                        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true"
                                                                                                          class="s7-close"></span>
                        </button>
                        <span class="icon s7-check"></span> {{ session('message') }}
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- JavaScripts -->
@yield('js-localization.head')
@if($app->environment('local'))
    <script src="{{ asset('assets/js/vendor_admin.js') }}"></script>
    <script src="{{ asset('assets/js/admin.js') }}"></script>
    <script src="{{ asset('assets/js/bundle.js') }}"></script>
@else
    <script src="{{ elixir('assets/js/vendor_admin.js', '.') }}"></script>
    <script src="{{ elixir('assets/js/admin.js', '.') }}"></script>
    <script src="{{ elixir('assets/js/bundle.js', '.') }}"></script>
@endif
</body>
</html>