<div class="am-left-sidebar am-left-sidebar--clic">
    <div class="content">
        <div class="am-logo"></div>
        <ul class="sidebar-elements">
            <li><a href="{{ route('partners.index') }}"><i class="icon s7-cash"></i><span>@lang('menu.partners.index')</span></a></li>
            <li><a href="{{ route('feedback.index') }}"><i class="icon s7-comment"></i><span>@lang('menu.feedback.index')</span></a></li>
            <li><a href="{{ route('teachers.index') }}"><i class="icon s7-users"></i><span>@lang('menu.teachers.index')</span></a></li>
            <li><a href="{{ route('programs.index') }}"><i class="icon s7-study"></i><span>@lang('menu.programs.index')</span></a></li>
            {{--<li><a href="{{ url('tasks') }}"><i class="icon s7-check"></i><span>Tasks</span></a></li>--}}
            {{--<li><a href="{{ url('projects') }}"><i class="icon s7-photo-gallery"></i><span>Projects</span></a></li>--}}
            {{--<li class="parent"><a href="#"><i class="icon s7-user"></i><span>Contacts</span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li><a href="{{ route('clients.index') }}">Clients</a></li>--}}
            {{--<li><a href="{{ route('client-contacts.index') }}">Client Contacts</a></li>--}}
            {{--<li><a href="{{ route('addresses.index') }}">Addresses</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li><a href="{{ url('timesheets') }}"><i class="icon s7-timer"></i><span>Time</span></a></li>--}}
            {{--<li><a href="{{ url('team-members') }}"><i class="icon s7-users"></i><span>Team</span></a></li>--}}
            {{--<li><a href="{{ url('invoices') }}"><i class="icon s7-copy-file"></i><span>Invoices</span></a></li>--}}
            {{--<li class="parent"><a href="#"><i class="icon s7-piggy"></i><span>Cashflow</span></a>--}}
            {{--<ul class="sub-menu">--}}
            {{--<li><a href="{{ url('cashflow') }}">Cashflow</a></li>--}}
            {{--<li><a href="{{ url('income') }}">Income</a></li>--}}
            {{--<li><a href="{{ url('expenses') }}">Expenses</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li><a href="{{ url('reports') }}"><i class="icon s7-folder"></i><span>Reports</span></a></li>--}}
            {{--<li><a href="{{ url('folders') }}"><i class="icon s7-folder"></i><span>Folders</span></a></li>--}}
        </ul>
        <!--Sidebar bottom content-->
    </div>
</div>
