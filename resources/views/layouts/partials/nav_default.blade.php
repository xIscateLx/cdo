<nav class="navbar navbar-default navbar-fixed-top am-top-header">
    <div class="container-fluid">
        <div class="navbar-header">
            <div class="page-title">
                <span>Dashboard</span>
            </div>
            <a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed">
                <span
                        class="icon-bar">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            <a href="{{ url('admin') }}" class="navbar-brand"></a>
        </div>
        <a href="#"
           data-toggle="collapse"
           data-target="#am-navbar-collapse"
           class="am-toggle-top-header-menu collapsed">
            <span class="icon s7-angle-down"></span>
        </a>

        <div id="am-navbar-collapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right am-user-nav">
                <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                        class="dropdown-toggle"><span class="angle-down s7-angle-down"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ route('settings.user.edit') }}"><span
                                        class="icon s7-unlock"></span>@lang('auth.change_password')</a></li>
                        <li><a href="{{ url('admin/logout') }}"> <span class="icon s7-power"></span>@lang('auth.logout')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav am-nav-right">
                <li><a href="{{ url('/') }}">@lang('menu.homepage')</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                       class="dropdown-toggle">@lang('menu.settings')
                        <span class="angle-down s7-angle-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="{{ route('settings.default.edit') }}">@lang('menu.default_settings')</a></li>
                        <li><a href="{{ route('settings.icons.index') }}">@lang('menu.icons')</a></li>
                        <li><a href="{{ route('settings.images.index') }}">@lang('menu.images')</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>