<!DOCTYPE html>
<html lang="{{ $app->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CDO">
    <meta name="author" content="Rostislav Lyashev">
    <meta name="keywords" content="повышение квалификации, обучение в омске">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <meta name="yandex-verification" content="40aabac74d8239ff"/>
    <title>@yield('title')</title>

    @if($app->environment('local'))
        <link href="{{ asset('assets/css/vendor_app.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    @else
        <link href="{{ elixir('assets/css/vendor_app.css', '.') }}" rel="stylesheet">
        <link href="{{ elixir('assets/css/app.css', '.') }}" rel="stylesheet">
@endif
<!-- Global site tag (gtag.js) - Google Analytics -->
{{--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-39861217-1"></script>--}}
{{--    <script>--}}
{{--        window.dataLayer = window.dataLayer || [];--}}

{{--        function gtag() {--}}
{{--            dataLayer.push(arguments);--}}
{{--        }--}}

{{--        gtag('js', new Date());--}}

{{--        gtag('config', 'UA-39861217-1');--}}
{{--    </script>--}}
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function(d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter47083194 = new Ya.Metrika({
                        id: 47083194,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true,
                    });
                } catch (e) {
                }
            });
            var n = d.getElementsByTagName('script')[0], s = d.createElement('script'), f = function() {
                n.parentNode.insertBefore(s, n);
            };
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'https://mc.yandex.ru/metrika/watch.js';
            if (w.opera == '[object Opera]') {
                d.addEventListener('DOMContentLoaded', f, false);
            } else {
                f();
            }
        })(document, window, 'yandex_metrika_callbacks');
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/47083194" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" src="https://vk.com/js/api/share.js?93" charset="windows-1251"></script>
</head>

<body data-spy="scroll" data-target="#top-main-navbar" data-offset="81">
<div class="container">
    <div class="heading">
        @yield('heading')
    </div>
    <div class="content">
        @yield('content')
    </div>
</div>
<!-- JavaScripts -->
@if($app->environment('local'))
    <script src="{{ asset('assets/js/vendor_app.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
@else
    <script src="{{ elixir('assets/js/vendor_app.js', '.') }}"></script>
    <script src="{{ elixir('assets/js/app.js', '.') }}"></script>
@endif
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
