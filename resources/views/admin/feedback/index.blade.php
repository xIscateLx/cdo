@extends('layouts.admin')

@section('title')
    @lang('headers.feedback.index')
@endsection

@section('action-panel')
    <a href="{{ route('feedback.create') }}" class="btn btn-primary btn-sm">@lang('buttons.new')</a>
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.feedback.index')</li>
@endsection

@section('content')
    <div class="row">
        <!--Responsive table-->
        <div class="col-sm-12">
            <div class="widget widget-fullwidth widget-small">
                <div class="table-responsive noSwipe">
                    <table class="table table-striped table-fw-widget table-hover">
                        <thead>
                        <tr>
                            <th width="55%">@lang('fields.name')</th>
                            <th width="25%">@lang('fields.position')</th>
                            <th width="10%">@lang('fields.active')</th>
                            <th width="10%">@lang('fields.actions')</th>
                        </tr>
                        </thead>
                        <tbody class="no-border-x">
                        @forelse($feedbacks as $feedback)
                            <tr>
                                <td class="user-avatar"><img src="{{ $feedback->avatar->url('thumb') }}"> {{ $feedback->name }}</td>
                                <td>{{ $feedback->position }}</td>
                                <td>
                                    <switch-button name="active" :value="{{ $feedback->active }}"
                                                   id="active_{{ $feedback->id }}"
                                                   :switch-yes-no="true"
                                                   update-src="{{ route('feedback.update', ['feedback' => $feedback->id]) }}"
                                    ></switch-button>
                                </td>
                                <td id="feedback-remove-{{ $feedback->id }}">
                                    <a href="{{ route('feedback.edit', ['feedback' => $feedback->id]) }}">@lang('buttons.edit')</a>
                                    <a href="#" class="text-danger"
                                       data-toggle="confirmation"
                                       data-placement="left"
                                       data-container="#feedback-remove-{{ $feedback->id }}"
                                       data-btn-ok-label="@lang('buttons.remove')"
                                       data-btn-ok-class="btn-success"
                                       data-btn-cancel-label="@lang('buttons.cancel')"
                                       data-btn-cancel-class="btn-danger"
                                       data-remove="{{ route('feedback.destroy', ['feedback' => $feedback->id]) }}"
                                       data-title="@lang('messages.remove_title')"
                                       data-content="@lang('messages.remove_item')"
                                    >@lang('buttons.delete')</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    @lang('messages.no_feedback')
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation">
                {{ $feedbacks->links() }}
            </nav>
        </div>
    </div>
@endsection