@extends('layouts.admin')

@section('title')
    @if($partner->id)
        @lang('headers.partners.edit', ['partner' => $partner->name])
    @else
        @lang('headers.partners.create')
    @endif
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li><a href="{{ route('partners.index') }}">@lang('menu.partners.index')</a></li>
    @if($partner->id)
        <li class="active">@lang('headers.partners.edit', ['partner' => $partner->name])</li>
    @else
        <li class="active">@lang('headers.partners.create')</li>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST"
                  action="{{ $partner->id ? route('partners.update', ['partner' => $partner->id]) : route('partners.store') }}"
                  enctype="multipart/form-data">
                @if($partner->id)
                    {{ method_field('PUT') }}
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-full-alt4">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('image_file') ? 'has-error' : '' }}">
                                    <image-crop
                                            :view-mode="3"
                                            btn-change="@lang('buttons.change_image')"
                                            btn-ok="@lang('buttons.done')"
                                            preview="img-rounded"
                                            image-src="{{ $partner->brand_file_name ? $partner->brand->url() : '' }}"
                                    ></image-crop>
                                    @if ($errors->has('image_file'))
                                        <small class="help-block">
                                            {{ $errors->first('image_file') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">@lang('fields.name')</label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{ old('name') ?: $partner->name }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">
                                            {{ $errors->first('name') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                    <label for="link">@lang('fields.link')</label>
                                    <input type="text" id="link" name="link" class="form-control"
                                           value="{{ old('link') ?: $partner->link }}">
                                    @if ($errors->has('link'))
                                        <small class="help-block">
                                            {{ $errors->first('link') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                    <div class="am-checkbox">
                                        <input type="hidden" name="active" value="0">
                                        <input id="active" type="checkbox" name="active"
                                               @if($partner->active) checked="" @endif>
                                        <label for="active">@lang('fields.active')</label>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="@lang('buttons.save')" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
