@extends('layouts.admin')

@section('title')
    @lang('headers.partners.index')
@endsection

@section('action-panel')
    <a href="{{ route('partners.create') }}" class="btn btn-primary btn-sm">@lang('buttons.new')</a>
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.partners.index')</li>
@endsection

@section('content')
    <div class="row">
        <!--Responsive table-->
        <div class="col-sm-12">
            <div class="widget widget-fullwidth widget-small">
                <div class="table-responsive noSwipe">
                    <table class="table table-striped table-fw-widget table-hover">
                        <thead>
                        <tr>
                            <th width="55%">@lang('fields.name')</th>
                            <th width="25%">@lang('fields.link')</th>
                            <th width="10%">@lang('fields.active')</th>
                            <th width="10%">@lang('fields.actions')</th>
                        </tr>
                        </thead>
                        <tbody class="no-border-x">
                        @forelse($partners as $partner)
                            <tr>
                                <td><img src="{{ $partner->brand->url('thumb') }}"> {{ $partner->name }}</td>
                                <td><a href="{{ $partner->link }}" target="_blank">{{ $partner->link }}</a></td>
                                <td>
                                    <switch-button name="active" :value="{{ $partner->active }}"
                                                   id="active_{{ $partner->id }}"
                                                   :switch-yes-no="true"
                                                   update-src="{{ route('partners.update', ['partner' => $partner->id]) }}"
                                    ></switch-button>
                                </td>
                                <td id="partner-remove-{{ $partner->id }}">
                                    <a href="{{ route('partners.edit', ['partner' => $partner->id]) }}">@lang('buttons.edit')</a>
                                    <a href="#" class="text-danger"
                                       data-toggle="confirmation"
                                       data-placement="left"
                                       data-container="#partner-remove-{{ $partner->id }}"
                                       data-btn-ok-label="@lang('buttons.remove')"
                                       data-btn-ok-class="btn-success"
                                       data-btn-cancel-label="@lang('buttons.cancel')"
                                       data-btn-cancel-class="btn-danger"
                                       data-remove="{{ route('partners.destroy', ['partner' => $partner->id]) }}"
                                       data-title="@lang('messages.remove_title')"
                                       data-content="@lang('messages.remove_item')"
                                    >@lang('buttons.delete')</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    @lang('messages.no_partners')
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation">
                {{ $partners->links() }}
            </nav>
        </div>
    </div>
@endsection