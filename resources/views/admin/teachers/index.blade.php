@extends('layouts.admin')

@section('title')
    @lang('headers.teachers.index')
@endsection

@section('action-panel')
    <a href="{{ route('teachers.create') }}" class="btn btn-primary btn-sm">@lang('buttons.new')</a>
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.teachers.index')</li>
@endsection

@section('content')
    <div class="row">
        <!--Responsive table-->
        <div class="col-sm-12">
            <div class="widget widget-fullwidth widget-small">
                <div class="table-responsive noSwipe">
                    <table class="table table-striped table-fw-widget table-hover">
                        <thead>
                        <tr>
                            <th width="55%">@lang('fields.name')</th>
                            <th width="25%">@lang('fields.position')</th>
                            <th width="10%">@lang('fields.active')</th>
                            <th width="10%">@lang('fields.actions')</th>
                        </tr>
                        </thead>
                        <tbody class="no-border-x">
                        @forelse($teachers as $teacher)
                            <tr>
                                <td class="user-avatar"><img src="{{ $teacher->avatar->url('thumb') }}"> {{ $teacher->name }}</td>
                                <td>{{ $teacher->position }}</td>
                                <td>
                                    <switch-button name="active" :value="{{ $teacher->active }}"
                                                   id="active_{{ $teacher->id }}"
                                                   :switch-yes-no="true"
                                                   update-src="{{ route('teachers.update', ['teacher' => $teacher->id]) }}"
                                    ></switch-button>
                                </td>
                                <td id="teacher-remove-{{ $teacher->id }}">
                                    <a href="{{ route('teachers.edit', ['teacher' => $teacher->id]) }}">@lang('buttons.edit')</a>
                                    <a href="#" class="text-danger"
                                       data-toggle="confirmation"
                                       data-placement="left"
                                       data-container="#teacher-remove-{{ $teacher->id }}"
                                       data-btn-ok-label="@lang('buttons.remove')"
                                       data-btn-ok-class="btn-success"
                                       data-btn-cancel-label="@lang('buttons.cancel')"
                                       data-btn-cancel-class="btn-danger"
                                       data-remove="{{ route('teachers.destroy', ['teacher' => $teacher->id]) }}"
                                       data-title="@lang('messages.remove_title')"
                                       data-content="@lang('messages.remove_item')"
                                    >@lang('buttons.delete')</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    @lang('messages.no_teachers')
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation">
                {{ $teachers->links() }}
            </nav>
        </div>
    </div>
@endsection