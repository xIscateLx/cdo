@extends('layouts.admin')

@section('title')
    @if($teacher->id)
        @lang('headers.teachers.edit', ['teacher' => $teacher->name])
    @else
        @lang('headers.teachers.create')
    @endif
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li><a href="{{ route('teachers.index') }}">@lang('menu.teachers.index')</a></li>
    @if($teacher->id)
        <li class="active">@lang('headers.teachers.edit', ['teacher' => $teacher->name])</li>
    @else
        <li class="active">@lang('headers.teachers.create')</li>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST"
                  action="{{ $teacher->id ? route('teachers.update', ['teacher' => $teacher->id]) : route('teachers.store') }}"
                  enctype="multipart/form-data">
                @if($teacher->id)
                    {{ method_field('PUT') }}
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-full-alt4">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('image_file') ? 'has-error' : '' }}">
                                    <image-crop
                                            :aspect-ratio="1"
                                            :view-mode="3"
                                            btn-change="@lang('buttons.change_image')"
                                            btn-ok="@lang('buttons.done')"
                                            preview="img-circle"
                                            image-src="{{ $teacher->avatar_file_name ? $teacher->avatar->url() : '' }}"
                                    ></image-crop>
                                    @if ($errors->has('image_file'))
                                        <small class="help-block">
                                            {{ $errors->first('image_file') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">@lang('fields.name')</label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{ old('name') ?: $teacher->name }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">
                                            {{ $errors->first('name') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}">
                                    <label for="position">@lang('fields.position')</label>
                                    <input type="text" id="position" name="position" class="form-control"
                                           value="{{ old('position') ?: $teacher->position }}">
                                    @if ($errors->has('position'))
                                        <small class="help-block">
                                            {{ $errors->first('position') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('about') ? ' has-error' : '' }}">
                                    <label for="about">@lang('fields.about')</label>
                                    <textarea type="text" id="about" name="about"
                                              class="form-control without-resize tinymce-desc" rows="10">{{ old('about') ?: $teacher->about }}</textarea>
                                    @if ($errors->has('about'))
                                        <small class="help-block">
                                            {{ $errors->first('about') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                    <div class="am-checkbox">
                                        <input type="hidden" name="active" value="0">
                                        <input id="active" type="checkbox" name="active"
                                               @if($teacher->active) checked="" @endif>
                                        <label for="active">@lang('fields.active')</label>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="@lang('buttons.save')" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
