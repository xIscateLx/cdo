@extends('layouts.admin')

@section('title')
    @lang('headers.default.settings.edit')
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.default.settings.edit')</li>
@endsection

@section('content')
    <form method="POST"
          action="{{ route('settings.default.update') }}"
          enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('settings.default_section')
                    </div>
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('site_name') ? ' has-error' : '' }}">
                            <label for="site_name">@lang('settings.site_name')</label>
                            <textarea id="site_name" name="site_name" rows="2"
                                      class="form-control without-resize">{{ old('site_name') ?: (!empty($variables['site_name']) ? $variables['site_name'] : '' ) }}</textarea>
                            @if ($errors->has('site_name'))
                                <small class="help-block">
                                    {{ $errors->first('site_name') }}
                                </small>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('vk') ? ' has-error' : '' }}">
                            <label for="vk">@lang('settings.vk')</label>
                            <div class="input-group">
                                <div class="input-group-addon">http://vk.com/</div>
                                <input type="text" id="vk" name="vk"
                                       class="form-control"
                                       value="{{ old('vk') ?: (!empty($variables['vk']) ? $variables['vk'] : '' ) }}">
                                @if ($errors->has('vk'))
                                    <small class="help-block">
                                        {{ $errors->first('vk') }}
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('fb_enabled') ? ' has-error' : '' }}">
                            <div class="am-checkbox">
                                <input  type="hidden" name="fb_enabled" value="off">
                                <input id="fb_enabled" type="checkbox" name="fb_enabled"
                                       @if(!empty($variables['fb_enabled']) && $variables['fb_enabled'] === 'on') checked="" @endif>
                                <label for="fb_enabled">@lang('settings.fb_active')</label>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('fb') ? ' has-error' : '' }}">
                            <label for="fb">@lang('settings.fb')</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://www.facebook.com/</div>
                                <input type="text" id="fb" name="fb"
                                       class="form-control"
                                       value="{{ old('fb') ?: (!empty($variables['fb']) ? $variables['fb'] : '' ) }}">
                                @if ($errors->has('fb'))
                                    <small class="help-block">
                                        {{ $errors->first('fb') }}
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('inst_enabled') ? ' has-error' : '' }}">
                            <div class="am-checkbox">
                                <input  type="hidden" name="inst_enabled" value="off">
                                <input id="inst_enabled" type="checkbox" name="inst_enabled"
                                       @if(!empty($variables['inst_enabled']) && $variables['inst_enabled'] === 'on') checked="" @endif>
                                <label for="inst_enabled">@lang('settings.inst_active')</label>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('inst') ? ' has-error' : '' }}">
                            <label for="inst">@lang('settings.inst')</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://www.instagram.com/</div>
                                <input type="text" id="inst" name="inst"
                                        class="form-control"
                                        value="{{ old('inst') ?: (!empty($variables['inst']) ? $variables['inst'] : '' ) }}">
                                @if ($errors->has('inst'))
                                    <small class="help-block">
                                        {{ $errors->first('inst') }}
                                    </small>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('label') ? ' has-error' : '' }}">
                            <label for="label">@lang('settings.label')</label>
                            <input type="file" id="label" name="label"
                                   class="form-control"
                                   value="{{ old('label') ?: (!empty($variables['label']) ? $variables['label'] : '' ) }}">
                            @if ($errors->has('label'))
                                <small class="help-block">
                                    {{ $errors->first('label') }}
                                </small>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('footer') ? ' has-error' : '' }}">
                            <label for="footer">@lang('settings.footer')</label>
                            <textarea id="footer" name="footer" rows="6"
                                      class="form-control without-resize">{{ old('footer') ?: (!empty($variables['footer']) ? $variables['footer'] : '' ) }}</textarea>
                            @if ($errors->has('footer'))
                                <small class="help-block">
                                    {{ $errors->first('footer') }}
                                </small>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-full-alt4">
                    <div class="panel-heading">
                        @lang('settings.about_section')
                    </div>
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('about') ? ' has-error' : '' }}">
                            <label for="about">@lang('settings.about')</label>
                            <textarea id="about" name="about" rows="6"
                                      class="form-control without-resize tinymce-desc">{{ old('about') ?: (!empty($variables['about']) ? $variables['about'] : '' ) }}</textarea>
                            @if ($errors->has('about'))
                                <small class="help-block">
                                    {{ $errors->first('about') }}
                                </small>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('image_file') ? 'has-error' : '' }}">
                            <label for="image_file">@lang('settings.about_image')</label>
                            <image-crop
                                    :aspect-ratio="1"
                                    :view-mode="3"
                                    btn-change="@lang('buttons.change_image')"
                                    btn-ok="@lang('buttons.done')"
                                    image-src="{{ !empty($variables['about_image']) ? Storage::url($variables['about_image']) : '' }}"
                            ></image-crop>
                            @if(!empty($variables['about_image']))
                                <a href="#" class="btn btn-danger"
                                   data-toggle="confirmation"
                                   data-placement="left"
                                   data-container="body"
                                   data-btn-ok-label="@lang('buttons.remove')"
                                   data-btn-ok-class="btn-success"
                                   data-btn-cancel-label="@lang('buttons.cancel')"
                                   data-btn-cancel-class="btn-danger"
                                   data-remove="{{ route('settings.default.about.image.destroy') }}"
                                   data-title="@lang('messages.remove_title')"
                                   data-content="@lang('messages.remove_about_image')"
                                >@lang('buttons.delete')</a>
                            @endif
                            @if ($errors->has('image_file'))
                                <small class="help-block">
                                    {{ $errors->first('image_file') }}
                                </small>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="text-right">
                    <input type="submit" value="@lang('buttons.save')" class="btn btn-primary">
                </div>
            </div>
        </div>
    </form>
@endsection
