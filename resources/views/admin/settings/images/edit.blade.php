@extends('layouts.admin')

@section('title')
    @if($image->id)
        @lang('headers.images.edit', ['image' => $image->name])
    @else
        @lang('headers.images.create')
    @endif
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li><a href="{{ route('settings.images.index') }}">@lang('menu.images')</a></li>
    @if($image->id)
        <li class="active">@lang('headers.images.edit', ['image' => $image->name])</li>
    @else
        <li class="active">@lang('headers.images.create')</li>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST"
                  action="{{ $image->id ? route('settings.images.update', ['image' => $image->id]) : route('settings.images.store') }}"
                  enctype="multipart/form-data">
                @if($image->id)
                    {{ method_field('PUT') }}
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-full-alt4">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('image_file') ? 'has-error' : '' }}">
                                    <image-crop
                                            :aspect-ratio="2"
                                            :view-mode="3"
                                            btn-change="@lang('buttons.change_image')"
                                            btn-ok="@lang('buttons.done')"
                                            preview="img-rounded"
                                            image-src="{{ $image->image_file_name ? $image->image->url() : '' }}"
                                    ></image-crop>
                                    @if ($errors->has('image_file'))
                                        <small class="help-block">
                                            {{ $errors->first('image_file') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                    <div class="am-checkbox">
                                        <input type="hidden" name="active" value="0">
                                        <input id="active" type="checkbox" name="active" @if($image->active) checked="" @endif>
                                        <label for="active">@lang('fields.active')</label>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="@lang('buttons.save')" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
