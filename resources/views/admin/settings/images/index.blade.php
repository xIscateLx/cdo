@extends('layouts.admin')

@section('title')
    @lang('headers.images.index')
@endsection

@section('action-panel')
    <a href="{{ route('settings.images.create') }}" class="btn btn-primary btn-sm">@lang('buttons.new')</a>
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.images.index')</li>
@endsection

@section('content')
    <div class="row">
        <!--Responsive table-->
        <div class="col-sm-12">
            <div class="widget widget-fullwidth widget-small">
                <div class="table-responsive noSwipe">
                    <table class="table table-striped table-fw-widget table-hover">
                        <thead>
                        <tr>
                            <th width="25%">@lang('fields.image')</th>
                            <th width="10%">@lang('fields.active')</th>
                            <th width="10%">@lang('fields.actions')</th>
                        </tr>
                        </thead>
                        <tbody class="no-border-x">
                        @forelse($images as $image)
                            <tr>
                                <td><img src="{{ $image->image->url('thumb') }}"></td>
                                <td>
                                    <switch-button name="active" :value="{{ $image->active }}"
                                                   id="active_{{ $image->id }}"
                                                   :switch-yes-no="true"
                                                   update-src="{{ route('settings.images.update', ['image' => $image->id]) }}"
                                    ></switch-button>
                                </td>
                                <td id="image-remove-{{ $image->id }}">
                                    <a href="{{ route('settings.images.edit', ['image' => $image->id]) }}">@lang('buttons.edit')</a>
                                    <a href="#" class="text-danger"
                                       data-toggle="confirmation"
                                       data-placement="left"
                                       data-container="#image-remove-{{ $image->id }}"
                                       data-btn-ok-label="@lang('buttons.remove')"
                                       data-btn-ok-class="btn-success"
                                       data-btn-cancel-label="@lang('buttons.cancel')"
                                       data-btn-cancel-class="btn-danger"
                                       data-remove="{{ route('settings.images.destroy', ['image' => $image->id]) }}"
                                       data-title="@lang('messages.remove_title')"
                                       data-content="@lang('messages.remove_item')"
                                    >@lang('buttons.delete')</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    @lang('messages.no_images')
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation">
                {{ $images->links() }}
            </nav>
        </div>
    </div>
@endsection