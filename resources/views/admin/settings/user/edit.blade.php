@extends('layouts.admin')

@section('title')
    @lang('auth.change_password')
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('auth.change_password')</li>
@endsection

@section('content')
    <form method="POST"
          action="{{ route('settings.user.update') }}"
          enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group has-feedback lg left-feedback no-label{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">@lang('fields.password')</label>
                            <input id="password" type="password" class="form-control no-border input-lg rounded"
                                   name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback lg left-feedback no-label{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation">@lang('fields.re-password')</label>
                            <input id="password-confirm" type="password" class="form-control no-border input-lg rounded"
                                   name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                    class="btn btn-success btn-lg btn-perspective btn-block">@lang('auth.change_password')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection