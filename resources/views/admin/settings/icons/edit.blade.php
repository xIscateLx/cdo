@extends('layouts.admin')

@section('title')
    @if($icon->id)
        @lang('headers.icons.edit', ['icon' => $icon->name])
    @else
        @lang('headers.icons.create')
    @endif
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li><a href="{{ route('settings.icons.index') }}">@lang('menu.icons')</a></li>
    @if($icon->id)
        <li class="active">@lang('headers.icons.edit', ['icon' => $icon->name])</li>
    @else
        <li class="active">@lang('headers.icons.create')</li>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST"
                  action="{{ $icon->id ? route('settings.icons.update', ['icon' => $icon->id]) : route('settings.icons.store') }}"
                  enctype="multipart/form-data">
                @if($icon->id)
                    {{ method_field('PUT') }}
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">@lang('fields.name')</label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           value="{{ old('name') ?: $icon->name }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">
                                            {{ $errors->first('name') }}
                                        </small>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('machine_name') ? ' has-error' : '' }}">
                                    <label for="machine_name">@lang('fields.machine_name')</label>
                                    @if(!$icon->id)
                                        <div class="input-group">
                                            <div class="input-group-addon">#icon-</div>
                                            <input type="text" id="machine_name" name="machine_name"
                                                   class="form-control"
                                                   value="{{ old('machine_name') ?: $icon->machine_name }}">
                                        </div>
                                        @if ($errors->has('machine_name'))
                                            <small class="help-block">
                                                {{ $errors->first('machine_name') }}
                                            </small>
                                        @endif
                                    @else
                                        <p class="form-control-static">{{ $icon->machine_name }}</p>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('svg') ? ' has-error' : '' }}">
                                    <label for="svg">@lang('fields.svg')</label>
                                    <input type="file" id="svg" name="svg" class="form-control" accept=".svg"
                                           value="{{ old('svg') ?: ''}}">
                                    @if ($errors->has('svg'))
                                        <small class="help-block">
                                            {{ $errors->first('svg') }}
                                        </small>
                                    @endif
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="@lang('buttons.save')" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection