@extends('layouts.admin')

@section('title')
    @lang('headers.icons.index')
@endsection

@section('action-panel')
    <a href="{{ route('settings.icons.create') }}" class="btn btn-primary btn-sm">@lang('buttons.new')</a>
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li class="active">@lang('headers.icons.index')</li>
@endsection

@section('content')
    @if($icons->contains('processed', false))
        <div role="alert" class="alert alert-warning alert-dismissible">
            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true"
                                                                                              class="s7-close"></span>
            </button>
            <span class="icon s7-check"></span>@lang('messages.icons_on_processed')
        </div>
    @endif
    <div class="row">
        <!--Responsive table-->
        <div class="col-sm-12">
            <div class="widget widget-fullwidth widget-small">
                <div class="table-responsive noSwipe">
                    <table class="table table-striped table-fw-widget table-hover">
                        <thead>
                        <tr>
                            <th width="5%">@lang('fields.icon')</th>
                            <th width="55%">@lang('fields.name')</th>
                            <th>@lang('fields.processed')</th>
                            <th width="10%">@lang('fields.actions')</th>
                        </tr>
                        </thead>
                        <tbody class="no-border-x icons-list">
                        @forelse($icons as $icon)
                            <tr>
                                <td class="icon-sm">
                                    <svg class="icon">
                                        <use xlink:href="{{ asset('assets/images/icons.svg') }}{{ $icon->machine_name }}"></use>
                                    </svg></td>
                                <td>{{ $icon->name }}</td>
                                <td>{{ $icon->processed ? trans('messages.yes') : trans('messages.no') }}</td>
                                <td id="icon-remove-{{ $icon->id }}">
                                    <a href="{{ route('settings.icons.edit', ['icon' => $icon->id]) }}">@lang('buttons.edit')</a>
                                    <a href="#" class="text-danger"
                                       data-toggle="confirmation"
                                       data-placement="left"
                                       data-container="#icon-remove-{{ $icon->id }}"
                                       data-btn-ok-label="@lang('buttons.remove')"
                                       data-btn-ok-class="btn-success"
                                       data-btn-cancel-label="@lang('buttons.cancel')"
                                       data-btn-cancel-class="btn-danger"
                                       data-remove="{{ route('settings.icons.destroy', ['icon' => $icon->id]) }}"
                                       data-title="@lang('messages.remove_title')"
                                       data-content="@lang('messages.remove_item')"
                                    >@lang('buttons.delete')</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    @lang('messages.no_icons')
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <nav aria-label="Page navigation">
                {{ $icons->links() }}
            </nav>
        </div>
    </div>
@endsection