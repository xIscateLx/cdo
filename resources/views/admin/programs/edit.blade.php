@extends('layouts.admin')

@section('title')
    @if($program->id)
        @lang('headers.programs.edit', ['program' => $program->name])
    @else
        @lang('headers.programs.create')
    @endif
@endsection

@section('breadcrump')
    <li><a href="{{ url('admin') }}"><span class="s7-home"></span></a></li>
    <li><a href="{{ route('programs.index') }}">@lang('menu.programs.index')</a></li>
    @if($program->id)
        <li class="active">@lang('headers.programs.edit', ['program' => $program->name])</li>
    @else
        <li class="active">@lang('headers.programs.create')</li>
    @endif
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <program
                    action="{{ $program->id ? route('programs.update', ['program' => $program->id]) : route('programs.store') }}"
                    icon-src="{{ asset('assets/images/icons.svg') }}"
                    method="{{ $program->id ? 'PUT' : 'POST' }}"
                    @if ($program->id) :program-id="{{ $program->id }}" @endif
            ></program>
        </div>
    </div>

@endsection