<div class="panel panel-section">
    <div class="panel-heading">О центре</div>
    <div class="panel-body">
        @if(!empty($variables['about_image']))
            <div class="row-items-2 right-row hidden-xs">
                <img src="{{ !empty($variables['about_image']) ? Storage::url($variables['about_image']) : '' }}"
                     class="image">
            </div>
            <div class="row-items-2">
                @else
                    <div>
                        @endif
                        <p class="text-default">
                            {!! !empty($variables['about']) ? nl2br($variables['about']) : '' !!}
                        </p>
                    </div>

            </div>
    </div>
    <div class="panel panel-section">
        <div class="panel-heading">Наши преимущества</div>
        <div class="panel-body">
            <div class="icon-items col">
                <div class="item">
                    <div class="circle-grey icon-bg">
                        <svg class="icon">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-people"></use>
                        </svg>
                    </div>
                    профессиональный преподавательский состав
                </div>
                <div class="item">
                    <div class="circle-grey icon-bg">
                        <svg class="icon icon-pennote">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-notepen"></use>
                        </svg>
                    </div>
                    широкий выбор актуальных программ
                </div>
                <div class="item">
                    <div class="circle-grey icon-bg">
                        <svg class="icon">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-student-cup"></use>
                        </svg>
                    </div>
                    престижность диплома и вуза
                </div>
                <div class="item">
                    <div class="circle-grey icon-bg">
                        <svg class="icon">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-tehnical"></use>
                        </svg>
                    </div>
                    современные методы и технологии в обучении
                </div>
                <div class="item">
                    <div class="circle-grey icon-bg">
                        <svg class="icon">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-handshake"></use>
                        </svg>
                    </div>
                    ориентированность на клиентов
                </div>
            </div>
        </div>
    </div>
</div>