<div class="contacts-section">
    <div class="share">
        <div class="inline">
            <script type="text/javascript">
                document.write(VK.Share.button({
                    'image': "{{ (!empty($variables['label']) ? asset(Storage::url($variables['label'])) : '' ) }}"
                }, {
                    type: 'round_nocount',
                    text: 'Поделиться'
                }));
            </script>
        </div>
{{--        <div class="inline">--}}
{{--            <div class="fb-share-button" data-layout="button" data-size="small" data-mobile-iframe="true"><a--}}
{{--                        class="fb-xfbml-parse-ignore" target="_blank"--}}
{{--                        href="https://www.facebook.com/sharer/sharer.php?u&amp;src=sdkpreparse">Поделиться</a></div>--}}
{{--        </div>--}}
        <div class="inline">
            <div id="ok_shareWidget"></div>
            <script>
                !function (d, id, did, st, title, description, image) {
                    var js = d.createElement("script");
                    js.src = "https://connect.ok.ru/connect.js";
                    js.onload = js.onreadystatechange = function () {
                        if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                            if (!this.executed) {
                                this.executed = true;
                                setTimeout(function () {
                                    OK.CONNECT.insertShareWidget(id,did,st, title, description, image);
                                }, 0);
                            }
                        }};
                    d.documentElement.appendChild(js);
                }(document,"ok_shareWidget","",'{"sz":20,"st":"rounded","nc":1,"ck":2}',"","","");
            </script>
        </div>
{{--        <div class="inline">--}}
{{--            <a href="https://twitter.com/share" class="twitter-share-button">Поделиться</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>--}}
{{--        </div>--}}
    </div>
    <div class="contacts">
        <div class="pull-right bottom icons">
            <img src="{{ asset('assets/images/omgu.png') }}">
        </div>
        <p>{!! !empty($variables['footer']) ? nl2br($variables['footer']) : '' !!}
        </p>
    </div>
</div>
