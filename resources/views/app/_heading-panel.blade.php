<div class="header clearfix">
    <div class="header-right">
        <div class="header-row clearfix">
            <div class="icons">
                <a href="http://omsu.ru"><img src="{{ asset('assets/images/omgu.png') }}"></a>
                <a href="http://vk.com/{{ $variables['vk'] or '' }}">
                    <img class="small" src="{{ asset('assets/images/vk.png') }}">
                </a>
                @if (!empty($variables['fb_enabled']) && $variables['fb_enabled'] === 'on')
                    <a href="https://www.facebook.com/{{ $variables['fb'] or '' }}">
                        <img class="small" src="{{ asset('assets/images/fb.png') }}">
                    </a>
                @endif
                @if (!empty($variables['inst_enabled']) && $variables['inst_enabled'] === 'on')
                    <a href="https://www.instagram.com/{{ $variables['inst'] or '' }}">
                        <img class="small" src="{{ asset('assets/images/inst.png') }}">
                    </a>
                @endif
            </div>
        </div>
        <div class="header-phone">
            <div class="header-row">
                8 (3812) 22-46-06
            </div>
            <div class="header-row">
                8 (3812) 20-98-52
            </div>
        </div>
    </div>
    <div class="brand">
        <a href="/">
            <img src="{{ (!empty($variables['label']) ? Storage::url($variables['label']) : '' ) }}" alt=""
                 class="logo">
        </a>
        {!! !empty($variables['site_name']) ? nl2br($variables['site_name']) : '' !!}
    </div>
</div>
<div class="top-main-navbar">
    <nav class="navbar navbar-default no-border">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-target="#top-main-navbar" aria-expanded="false">
                <span class="fa fa-angle-right"></span>
            </button>
        </div>
        <div class="navbar-collapse" id="top-main-navbar">
            <ul class="nav navbar-nav" role="tablist">
                <li class="no-wrap">
                    <a href="#about">
                        О центре
                    </a>
                </li>
                <li>
                    <a href="#programs">
                        Программы обучения
                    </a>
                </li>
                <li>
                    <a href="#teachers">
                        Преподаватели
                    </a>
                </li>
                <li>
                    <a href="#start-training">
                        Приступить к обучению
                    </a>
                </li>
                <li>
                    <a href="#contacts">
                        Контакты
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

