<div class="panel panel-section program">
    <div class="panel-heading">Программы обучения</div>
    <div class="panel-body">
        <div class="icon-items program-icons">
            @foreach($programs as $program)
                <div class="item item-sm" data-toggle="#program-{{ $program->id }}">
                    <div class="circle-grey icon-sm">
                        <svg class="icon">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}{{ $program->icon ? $program->icon->machine_name : '' }}"></use>
                        </svg>
                    </div>
                    {{ $program->name }}
                </div>
            @endforeach
        </div>
        <div class="program-list">
            @foreach($programs as $program)
                @if($program->items->count() > 0)
                    <div class="program-info clearfix" id="program-{{ $program->id }}">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <div class="program-toolbar">
                            @if($program->items->count() > 1)
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($program->items as $item)
                                        <li class="{{ $loop->first ? 'active' : '' }}">
                                            <a class="btn btn-default btn-program" data-toggle="tab"
                                               href="#program-{{ $program->id }}-{{ $item->id }}">
                                                @if($item->icon)
                                                    <svg class="transparent background">
                                                        <use xlink:href="{{ asset('assets/images/icons.svg') }}{{ $item->icon->machine_name }}"></use>
                                                    </svg>
                                                @endif
                                                <p>{{ $item->name }}</p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <div class="tab-content">
                            @foreach($program->items as $item)
                                <div id="program-{{ $program->id }}-{{ $item->id }}"
                                     class="tab-pane in fade {{ $loop->first ? 'active' : '' }}">
                                    {!! $item->about !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="panel panel-section">
    <div class="panel-heading">Отзывы</div>
    <div class="panel-body">
        @if($feedbacks->count() > 0)
            <div id="carousel-reports" class="carousel slide" data-ride="carousel" data-interval="8500">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @for ($i = 0; $i < $feedbacks->count(); $i++)
                        <li data-target="#carousel-reports" data-slide-to="{{ $i }}"
                            @if($i === 0) class="active" @endif></li>
                    @endfor
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner transparent" role="listbox">
                    @foreach($feedbacks as $feedback)
                        <div class="item {{ $loop->first ? 'active' : '' }}">
                            <div class="the-box no-border">
                                <div class="text-center">
                                    <img src="{{ $feedback->avatar->url('avatar') }}"
                                         class="img-circle with-margin avatar">
                                    <p class="bolded text-uppercase">
                                        {{ $feedback->name }}
                                    </p>
                                    <p class="bolded">
                                        {{ $feedback->position }}
                                    </p>
                                </div>
                                {!! $feedback->message !!}
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-reports" role="button" data-slide="prev">
                    <svg class="button prev-button">
                        <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-prev"></use>
                    </svg>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-reports" role="button" data-slide="next">
                    <svg class="button next-button">
                        <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-next"></use>
                    </svg>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        @endif
    </div>
</div>