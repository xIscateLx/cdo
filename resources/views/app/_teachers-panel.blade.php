<div class="panel panel-section">
    <div class="panel-heading">Преподаватели</div>
    <div class="panel-body">
        @if($teachers->count() > 0)
            <div id="carousel-teachers" class="carousel slide" data-ride="carousel" data-interval="10000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @for ($i = 0; $i < $teachers->count(); $i++)
                        <li data-target="#carousel-teachers" data-slide-to="{{ $i }}"
                            @if($i == 0) class="active" @endif></li>
                    @endfor
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner transparent" role="listbox">
                    @foreach($teachers as $teacher)
                        <div class="item {{ $loop->first ? 'active' : '' }}">
                            <div class="the-box no-border">
                                <div class="text-center">
                                    <img src="{{ $teacher->avatar->url('avatar') }}"
                                         class="img-circle with-margin avatar">
                                    <p class="bolded text-uppercase">
                                        {{ $teacher->name }}
                                    </p>
                                    <p class="bolded">
                                        {{ $teacher->position }}
                                    </p>
                                </div>
                                {!!  $teacher->about  !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            @if ($teachers->count() > 0)
                <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-teachers" role="button" data-slide="prev">
                        <svg class="button prev-button">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-prev"></use>
                        </svg>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-teachers" role="button" data-slide="next">
                        <svg class="button next-button">
                            <use xlink:href="{{ asset('assets/images/icons.svg') }}#icon-next"></use>
                        </svg>
                        <span class="sr-only">Next</span>
                    </a>
                @endif
            </div>
        @endif
    </div>
</div>
<div class="panel panel-section">
    <div class="panel-heading">Наши партнеры</div>
    <div class="panel-body">
        <div class="icon-items no-wrap">
            @foreach($partners as $partner)
                <div class="item">
                    <a href="{{ $partner->link }}">
                        <img class="icon-image" src="{{ $partner->brand->url('brand') }}"
                             alt="{{ $partner->name }}">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>