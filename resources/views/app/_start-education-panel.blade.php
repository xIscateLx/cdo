<div class="center-block start-training">
    <div class="background-image">
        <button type="button" class="btn btn-warning btn-lg btn-block transparent start-text" data-toggle="modal"
                data-target="#contact_form">Приступить к обучению
        </button>
    </div>
</div>

<div class="modal fade  training-modal" tabindex="-1" role="dialog" id="contact_form">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Оставьте ваши контактные
                    данные, чтобы специалист центра мог связаться с вами</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url('/') }}" method="POST" id="student-request-form">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="full_name" placeholder="{{ trans('forms.full_name') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control bfh-phone" name="phone" data-format="+7 (ddd) ddd-dddd" placeholder="{{ trans('forms.phone') }}">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="{{ trans('forms.email') }}">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-warning btn-submit btn-block" id="send-student-request">{{ trans('forms.send') }}</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->