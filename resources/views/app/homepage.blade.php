@extends('layouts.app')

@section('title')
    Центр делового образования
@endsection

@section('heading')
    @include('app._heading-panel')
@endsection

@section('content')
    <div id="top">
        @if($images->count() > 0)
            <div id="carousel-top-images" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @for ($i = 0; $i < $images->count(); $i++)
                        <li data-target="#carousel-top-images" data-slide-to="{{ $i }}"
                            @if($i == 0) class="active" @endif></li>
                    @endfor
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner transparent" role="listbox">
                    @foreach($images as $image)
                        <div class="item {{ $loop->first ? 'active' : '' }}">
                            <img src="{{ $image->image->url('pub') }}" alt="{{ $loop->iteration }}">
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div id="about">
        @include('app._about-panel')
    </div>
    <div id="programs">
        @include('app._programs-panel')
    </div>
    <div id="teachers">@include('app._teachers-panel')</div>
    <div id="start-training">@include('app._start-education-panel')</div>
    <div id="contacts">@include('app._contacts-panel')</div>
@endsection