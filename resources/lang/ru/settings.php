<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.03.17
 * Time: 10:07
 */

return [
    'default_section' => 'Общий раздел',
    'site_name'       => 'Имя сайта',
    'vk'              => 'VК адрес',
    'fb'              => 'Facebook адрес',
    'inst'            => 'Instagram адрес',
    'inst_active'              => 'Показывать Instagram',
    'fb_active'              => 'Показывать Facebook',
    'label'           => 'Брэнд',
    'footer'          => 'Футер',
    'about_section'   => 'Раздел "О нас"',
    'about'           => 'О нас',
    'about_image'     => 'Изображение',
];

