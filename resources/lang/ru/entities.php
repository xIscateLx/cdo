<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 16.02.17
 * Time: 14:25
 */

return [
    'partner'          => 'Партнер',
    'feedback'         => 'Отзыв',
    'teacher'          => 'Преподаватель',
    'program'          => 'Программа',
    'icon'             => 'Иконка',
    'image'            => 'Изображение',
    'default_settings' => 'Общие настройки',
    'about_image'      => 'Изображение "О нас"',
    'user'             => 'Пользователь',
];