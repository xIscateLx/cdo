<?php

/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 07.02.17
 * Time: 15:22
 */

return [
    'change_image' => 'Изменить изображение',
    'new' => 'Добавить',
    'save' => 'Сохранить',
    'done' => 'Готово',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'remove' => 'Удалить',
    'cancel' => 'Отменить',
    'close' => 'Закрыть'
];