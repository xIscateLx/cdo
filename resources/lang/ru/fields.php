<?php

return [
    'email'        => 'Почта',
    'password'     => 'Пароль',
    're-password'  => 'Повторите пароль',
    'remember'     => 'Запомнить меня',
    'name'         => 'Наименование',
    'link'         => 'Ссылка',
    'active'       => 'Активно?',
    'actions'      => 'Действия',
    'position'     => 'Должность',
    'message'      => 'Сообщение',
    'about'        => 'Описание',
    'machine_name' => 'Машинное имя',
    'svg'          => 'SVG Иконка',
    'processed'    => 'Обработано',
    'icon'         => 'Иконка',
    'image'        => 'Изображение',
];