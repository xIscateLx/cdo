<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.02.17
 * Time: 10:41
 */

return [
    'homepage'         => 'Сайт',
    'settings'         => 'Настройки',
    'partners.index'   => 'Партнеры',
    'feedback.index'   => 'Отзывы',
    'teachers.index'   => 'Учителя',
    'programs.index'   => 'Программы',
    'icons'            => 'Иконки',
    'images'           => 'Изображения',
    'default_settings' => 'Общие настройки',
];