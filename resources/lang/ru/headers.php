<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.02.17
 * Time: 13:19
 */

return [
    'partners.index'        => 'Партнеры',
    'partners.create'       => 'Добавить партнера',
    'partners.edit'         => 'Редакатировать :partner',
    'partners.show'         => 'Просмотр :partner',
    'feedback.index'        => 'Отзывы',
    'feedback.create'       => 'Добавить отзыв',
    'feedback.edit'         => 'Редактировать отзыв',
    'teachers.index'        => 'Преподаватели',
    'teachers.create'       => 'Добавить преподавателя',
    'teachers.edit'         => 'Редактировать :teacher',
    'programs.index'        => 'Программы',
    'programs.create'       => 'Добавить программу',
    'programs.edit'         => 'Редактировать :program',
    'program.items.index'   => 'Направления',
    'program.items.create'  => 'Добавить направление',
    'program.items.edit'    => 'Редактировать :item',
    'icons.index'           => 'Иконки',
    'icons.create'          => 'Добавить иконку',
    'icons.edit'            => 'Редактировать иконку',
    'images.index'          => 'Изображения',
    'images.create'         => 'Добавить изображение',
    'images.edit'           => 'Редактировать изображение',
    'default.settings.edit' => 'Редактировать общие настройки',
];