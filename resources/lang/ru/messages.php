<?php

return [
    'hello'                        => 'Привет!',
    'whoops'                       => 'Упс!',
    'regards'                      => 'С уважением',
    'trouble_clicking'             => 'Если у вас возникли проблемы, нажав на кнопку ":text", скопируйте и вставьте URL ниже в свой веб-браузер:',
    'right_reserved'               => 'Все права защищены.',
    'no_partners'                  => 'Партнеры не добавлены.',
    'remove_title'                 => 'Удалить запись?',
    'remove_item'                  => 'Вы действительно хотите удалить эту запись?',
    'create_successful'            => ':entity успешно добавлен!',
    'edit_successful'              => ':entity успешно обновлен!',
    'delete_successful'            => ':entity успешно удален!',
    'no_feedback'                  => 'Отзывы не добавлены.',
    'no_teachers'                  => 'Преподаватели не добавлены.',
    'no_programs'                  => 'Программы не добавлены.',
    'no_icons'                     => 'Иконки не добавлены.',
    'yes'                          => 'Да',
    'no'                           => 'Нет',
    'icons_on_processed'           => 'Внимание! Имеются иконки на обработке. Они будут обработаны в ближайшее время!',
    'no_program_items'             => 'Направления не добавлены.',
    'student_request_send_success' => 'Ваша заявка на обучение отправлена!',
    'no_images'                    => 'Изображения не добавлены',
    'remove_about_image'           => 'Вы действительно хотите удалить картинку описания?',
];