<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть не менее шести символов и соответствовать подтверждению.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отправили ссылку восстановления пароля по электронной почте!',
    'token' => 'Этот маркер сброса пароля является недействительным.',
    'user' => "Пользователь с этой электронной почтой не найден.",
    'passwor_reset_email_receiving' => 'Вы получили это письмо, потому что мы получили запрос на изменение пароля для учетной записи.',
    'reset_password_action' => 'Сбросить пароль',
    'passwor_reset_dont_request' => 'Если вы не отправляли запрос на изменение пароля, не требуется никаких дополнительных действий.',
    'reset_email' => 'Отправить ссылку сброса пароля',
    'reset_email_notice' => 'Введите Ваш адрес электронной почты для восстановления пароля',
    'reset_password' => 'Восстановить пароль',
];
