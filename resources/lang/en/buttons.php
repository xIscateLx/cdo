<?php

/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 07.02.17
 * Time: 15:22
 */

return [
    'change_image' => 'Change Image',
    'new' => 'Add new',
    'save' => 'Save',
    'done' => 'Done',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'remove' => 'Remove',
    'cancel' => 'Cancel',
    'close' => 'Close',
];