<?php

return [
    'email'        => 'Email',
    'password'     => 'Password',
    're-password'  => 'Re-enter password',
    'remember'     => 'Remember me',
    'name'         => 'Name',
    'link'         => 'Link',
    'active'       => 'Active',
    'actions'      => 'Actions',
    'position'     => 'Position',
    'message'      => 'Message',
    'about'        => 'About',
    'machine_name' => 'Machine name',
    'svg'          => 'SVG Icon',
    'processed'    => 'Processed',
    'icon'         => 'Icon',
    'image'        => 'Image',
];