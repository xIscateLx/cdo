<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.03.17
 * Time: 10:07
 */

return [
    'default_section' => 'Default section',
    'site_name'       => 'Site name',
    'vk'              => 'VK url',
    'fb'              => 'Facebook url',
    'label'           => 'Site brand',
    'footer'          => 'Site footer',
    'about_section'   => 'About section',
    'about'           => 'About',
    'about_image'     => 'About image',
];