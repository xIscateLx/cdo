<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.02.17
 * Time: 13:19
 */

return [
    'partners.index'        => 'Partners',
    'partners.create'       => 'Add Partner',
    'partners.edit'         => 'Edit :partner',
    'partners.show'         => 'Show :partner',
    'feedback.index'        => 'Feedback',
    'feedback.create'       => 'Add Feedback',
    'feedback.edit'         => 'Edit Feedback',
    'teachers.index'        => 'Teachers',
    'teachers.create'       => 'Add Teacher',
    'teachers.edit'         => 'Edit :teacher',
    'programs.index'        => 'Programs',
    'programs.create'       => 'Add Program',
    'programs.edit'         => 'Edit :program',
    'program.items.index'   => 'Program Items',
    'program.items.create'  => 'Add Program Item',
    'program.items.edit'    => 'Edit :item',
    'icons.index'           => 'Icons',
    'icons.create'          => 'Add Icon',
    'icons.edit'            => 'Edit icon',
    'images.index'          => 'Images',
    'images.create'         => 'Add Images',
    'images.edit'           => 'Edit Images',
    'default.settings.edit' => 'Edit default settings',
];