<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 16.02.17
 * Time: 14:25
 */

return [
    'partner'          => 'Partner',
    'feedback'         => 'Feedback',
    'teacher'          => 'Teacher',
    'program'          => 'Program',
    'icon'             => 'Icon',
    'image'            => 'Image',
    'default_settings' => 'Default settings',
    'about_image'      => 'About image',
    'user'             => 'User',
];