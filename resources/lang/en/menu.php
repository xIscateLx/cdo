<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.02.17
 * Time: 10:41
 */

return [
    'homepage'         => 'Homepage',
    'settings'         => 'Settings',
    'partners.index'   => 'Partners',
    'feedback.index'   => 'Feedback',
    'teachers.index'   => 'Teachers',
    'programs.index'   => 'Programs',
    'icons'            => 'Icons',
    'images'           => 'Images',
    'default_settings' => 'Default settings',
];