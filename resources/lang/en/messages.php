<?php

return [
    'hello'                        => 'Hello!',
    'whoops'                       => 'Whoops!',
    'regards'                      => 'Regards',
    'trouble_clicking'             => 'If you’re having trouble clicking the ":text" button, copy and paste the URL below into your web browser:',
    'right_reserved'               => 'All rights reserved.',
    'no_partners'                  => 'No partners exists',
    'remove_title'                 => 'Remove item?',
    'remove_item'                  => 'You are really want remove this item?',
    'create_successful'            => 'The :entity successful created!',
    'edit_successful'              => 'The :entity successful edited!',
    'delete_successful'            => 'The :entity successful deleted!',
    'no_feedback'                  => 'No feedback exists',
    'no_teachers'                  => 'No teachers exists',
    'no_programs'                  => 'No programs exists',
    'no_icons'                     => 'No icons exists',
    'yes'                          => 'Yes',
    'no'                           => 'No',
    'icons_on_processed'           => 'Warning! Icons on precessed exists!',
    'no_program_items'             => 'No program items exists',
    'student_request_send_success' => 'You request successful send',
    'no_images'                    => 'No images exists',
    'remove_about_image'           => 'You are really want remove about image?',
];