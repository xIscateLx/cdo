<?php

Route::get('/', 'HomePageController@homepage');

Route::post('/', 'HomePageController@sendNotification');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', 'LoginController@showLoginForm');
        Route::post('login', 'LoginController@login');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
        Route::post('password/reset', 'ResetPasswordController@reset');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
        Route::get('logout', 'LoginController@logout');
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', function () {
            return view('layouts.admin');
        });

        Route::group(['as' => 'settings.', 'namespace' => 'Settings', 'prefix' => 'settings'], function () {
            Route::get('/default', ['as' => 'default.edit', 'uses' => 'DefaultSettingsController@edit']);
            Route::put('/default', ['as' => 'default.update', 'uses' => 'DefaultSettingsController@update']);
            Route::delete('/default/about_image',
                ['as' => 'default.about.image.destroy', 'uses' => 'DefaultSettingsController@aboutImageDestroy']);
            Route::resource('icons', 'IconController');
            Route::resource('images', 'ImagesController');

            Route::get('user', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
            Route::put('user', ['as' => 'user.update', 'uses' => 'UserController@update']);
        });

        Route::resource('partners', 'PartnersController', [
            'except' => ['show'],
        ]);

        Route::resource('feedback', 'FeedbackController', [
            'except' => ['show'],
        ]);

        Route::resource('teachers', 'TeachersController', [
            'except' => ['show'],
        ]);

        Route::put('programs/{program}/status',
            ['as' => 'programs.status', 'uses' => 'ProgramsController@updateStatus']);
        Route::resource('programs', 'ProgramsController');
    });
});
