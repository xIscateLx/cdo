const elixir = require('laravel-elixir');
require('laravel-elixir-ng-annotate');
require('laravel-elixir-remove');
require('laravel-elixir-vue-2');

var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var path = require('path');
var cheerio = require('gulp-cheerio');
var rename = require('gulp-rename');

var bowerDir = './vendor/bower_components/';

var vendorScripts = {
    app: [
        bowerDir + 'jquery/dist/jquery.min.js',
        bowerDir + 'bootstrap-sass/assets/javascripts/bootstrap.js',
        bowerDir + 'chosen/chosen.jquery.js',
        bowerDir + 'svg4everybody/dist/svg4everybody.js',
        bowerDir + 'slick-carousel/slick/slick.js',
        bowerDir + 'jquery.gritter/js/jquery.gritter.js',
        bowerDir + 'bootstrap-formhelpers/dist/js/bootstrap-formhelpers.js',
        bowerDir + 'bootstrap-formhelpers/js/bootstrap-formhelpers-phone.js',
    ],
    admin: [
        bowerDir + 'jquery/dist/jquery.min.js',
        bowerDir + 'bootstrap-sass/assets/javascripts/bootstrap.js',
        bowerDir + 'svg4everybody/dist/svg4everybody.js',
        bowerDir + 'cropperjs/dist/cropper.js',
        bowerDir + 'nanoscroller/bin/javascripts/jquery.nanoscroller.js',
        bowerDir + 'bootstrap-confirmation2/bootstrap-confirmation.js',
        bowerDir + 'jquery.gritter/js/jquery.gritter.js',
        bowerDir + 'select2/dist/js/select2.js',
        bowerDir + 'tinymce/tinymce.js',
        bowerDir + 'tinymce/themes/modern/theme.min.js',
    ]
};

var appScripts = [
    'app.js',
    'app'
];

var adminScripts = [
    'admin.js',
    'admin'
];

elixir(function (mix) {
    mix
    //.remove('public/assets')
        .task('svgstore')
        // Styles APP
        .sass(['vendor.scss'], 'public/assets/css/vendor_app.css', null, {includePaths: bowerDir})
        .sass(['app.scss'], 'public/assets/css/app.css')

        // Styles Admin
        .less(['admin.less'], 'public/assets/css/admin.css', null, {paths: ['.', bowerDir]})
        .less(['vendor.less'], 'public/assets/css/vendor_admin.css', null, {paths: ['.', bowerDir]})

        // Copy fonts
        .copy(bowerDir + 'bootstrap-sass/assets/fonts/bootstrap', 'public/assets/fonts/bootstrap')
        .copy(bowerDir + 'font-awesome/fonts', 'public/assets/fonts/font-awesome')
        .copy(bowerDir + 'slick-carousel/slick/fonts', 'public/assets/css/fonts/')
        .copy('./resources/assets/fonts/open-sans', 'public/assets/css/fonts/open-sans')
        .copy('./resources/assets/fonts/raleway', 'public/assets/css/fonts/raleway')
        .copy('./resources/assets/fonts/core-sans', 'public/assets/css/fonts/core-sans')
        .copy('./resources/assets/vendor/stroke-7/fonts', 'public/assets/css/fonts')

        // Copy images
        .copy('./resources/assets/images', 'public/assets/images')
        .copy('./resources/assets/vendor', 'public/assets/vendor')
        .copy(bowerDir + 'cropperjs/src/images', 'public/assets/images')
        .copy(bowerDir + 'slick-carousel/slick/ajax-loader.gif', 'public/assets/css/ajax-loader.gif')
        .copy(bowerDir + 'jquery.gritter/images', 'public/assets/images')

        // TinyMCE
        .copy(bowerDir + 'tinymce/skins', 'public/assets/js/skins')
        .copy(bowerDir + 'tinymce/plugins', 'public/assets/js/plugins');

    if (elixir.config.production) {
        // Disable creation of sourcemap files
        elixir.config.sourcemaps = false;

        // Scripts App
        mix.scripts(vendorScripts.app, 'public/assets/js/vendor_app.js');
        mix.annotate(appScripts, 'public/assets/js/app.annotated.js').scripts('app.annotated.js', 'public/assets/js/app.js', 'public/assets/js/');
        mix.remove('public/assets/js/app.annotated.js');


        // Scripts Admin
        mix.scripts(vendorScripts.admin, 'public/assets/js/vendor_admin.js');
        mix.annotate(adminScripts, 'public/assets/js/admin.annotated.js').scripts('admin.annotated.js', 'public/assets/js/admin.js', 'public/assets/js/');
        mix.remove('public/assets/js/admin.annotated.js');

        mix.webpack('bundle.js', 'public/assets/js/bundle.js')



        // Add versions to assets
            .version([
                'assets/js/vendor_app.js',
                'assets/js/vendor_admin.js',
                'assets/js/admin.js',
                'assets/js/app.js',
                'assets/js/bundle.js',
                'assets/css/vendor_app.css',
                'assets/css/vendor_admin.css',
                'assets/css/admin.css',
                'assets/css/app.css'
            ], 'public/');
    } else {
        mix
        // Scripts App
            .scripts(vendorScripts.app, 'public/assets/js/vendor_app.js')
            .scripts(appScripts, 'public/assets/js/app.js')

            // Scripts Admin
            .scripts(vendorScripts.admin, 'public/assets/js/vendor_admin.js')
            .scripts(adminScripts, 'public/assets/js/admin.js')
            .webpack('bundle.js', 'public/assets/js/bundle.js');
    }
});

var assetsDir = './storage/app/public/';
gulp.task('svgstore', function () {
    return gulp
        .src(assetsDir + 'images/icons/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix,
                        minify: true
                    }
                }]
            }
        }))
        .pipe(rename({prefix: 'icon-'}))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        .pipe(svgstore({inlineSvg: true}))
        .pipe(gulp.dest('public/assets/images/'));
});
