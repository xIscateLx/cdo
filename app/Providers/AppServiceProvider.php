<?php

namespace App\Providers;

use App\Models\Icon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_icon', function ($attribute, $value, $parameters, $validator) {
            $name = '#icon-' . $value;
            $icons = Icon::whereMachineName($name)->get();
            return !$icons->count();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
