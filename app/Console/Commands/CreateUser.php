<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create 
        {email : User email} 
        {name : User name} 
        {password : User password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = User::where('email', $email)->first();
        if ($user) {
            $this->error('This user was created!');
        } else {
            User::create([
                'name' => $this->argument('name'),
                'email' => $this->argument('email'),
                'password' => bcrypt($this->argument('password')),
                'archive' => false,
            ]);
            $this->info('New user created!');
        }
    }
}
