<?php

namespace App\Console\Commands;

use App\Traits\ExecIconsUpdate;
use Illuminate\Console\Command;

class UpdateIcons extends Command
{
    use ExecIconsUpdate;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'icons:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update icons manual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = $this->runUpdate();
        if (0 === $status) {
            $this->error('Update icons failed!');
            return;
        }
        $this->info('Update icons successful!');
    }
}
