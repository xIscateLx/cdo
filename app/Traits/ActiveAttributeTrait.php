<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 20.02.17
 * Time: 12:51
 */

namespace App\Traits;


trait ActiveAttributeTrait
{
    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = ($value || $value == 'on') ? true : false;
    }
}