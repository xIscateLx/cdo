<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 27.02.17
 * Time: 12:56
 */

namespace App\Traits;


use App\Models\Icon;

trait ExecIconsUpdate
{
    public function runUpdate()
    {
        $result = exec('cd ' . base_path() . '; gulp svgstore;');
        if (preg_match('#Finished#', $result)) {
            Icon::whereProcessed(0)->update(['processed' => true]);
            return 1;
        }
        return 0;
    }
}