<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 20.02.17
 * Time: 13:09
 */

namespace App\Traits;

trait ProcessImageTrait
{
    /**
     * Process image
     *
     * @param mixed  $image
     * @param array  $cropData
     * @param string $extension
     *
     * @return \Intervention\Image\Image
     */
    protected function processImage($image, array $cropData = null, $extension = 'data-url')
    {
        $img = \Image::make($image);
        if ((int) $cropData['width'] == 0 && (int) $cropData['height'] == 0) {
            return $img->encode($extension);
        }
        $img->crop((int) $cropData['width'], (int) $cropData['height'], (int) $cropData['x'], (int) $cropData['y']);

        if ((int) $cropData['scaleX'] !== 1) {
            $img->flip('h');
        }
        if ((int) $cropData['scaleY'] !== 1) {
            $img->flip('v');
        }

        return $img->encode($extension);
    }
}