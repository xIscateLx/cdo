<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 20.02.17
 * Time: 12:51
 */

namespace App\Traits;


use App\Models\Icon;

trait IconAttributeTrait
{
    public function setIconAttribute($value)
    {
        $icon = Icon::whereMachineName($value)->first();
        $this->attributes['icon_id'] = $icon ? $icon->id : null;
    }
}