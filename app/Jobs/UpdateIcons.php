<?php

namespace App\Jobs;

use App\Models\Icon;
use App\Traits\ExecIconsUpdate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateIcons implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExecIconsUpdate;
    /**
     * @var Icon
     */
    protected $icon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Icon $icon)
    {
        $this->icon = $icon;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $icon = $this->icon->fresh();
        if ($icon->processed) {
            return;
        }
        $this->runUpdate();
    }
}
