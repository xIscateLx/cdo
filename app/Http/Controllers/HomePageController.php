<?php
/**
 * Created by PhpStorm.
 * User: Рост
 * Date: 16.02.2017
 * Time: 22:10
 */

namespace App\Http\Controllers;

use App\Http\Requests\SendStudentRequest;
use App\Mail\StudentRequested;
use App\Models\Feedback;
use App\Models\Image;
use App\Models\Partner;
use App\Models\Program;
use App\Models\Teacher;
use App\Models\Variable;

class HomePageController extends Controller
{
    /**
     *
     * @return Response
     */
    public function homepage()
    {
        return view('app.homepage', [
            'partners'  => Partner::whereActive(true)->get(),
            'feedbacks' => Feedback::whereActive(true)->get(),
            'teachers'  => Teacher::whereActive(true)->get(),
            'programs'  => Program::whereActive(true)->get(),
            'images'    => Image::whereActive(true)->get(),
            'variables' => Variable::all()->pluck('value', 'key')->toArray(),
        ]);
    }

    /**
     * Send notification
     *
     * @param SendStudentRequest $request
     *
     * @return array
     */
    public function sendNotification(SendStudentRequest $request)
    {
        \Mail::to(env('MAIL_FROM'))->send(new StudentRequested($request->all()));

        return ['message' => trans('messages.student_request_send_success')];
    }
}