<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTeachersRequest;
use App\Http\Requests\UpdateTeachersRequest;
use App\Models\Teacher;
use App\Traits\ProcessImageTrait;
use Illuminate\Http\Request;
use Storage;

class TeachersController extends Controller
{
    use ProcessImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.teachers.index', [
            'teachers' => Teacher::paginate(25),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teachers.edit', [
            'teacher' => new Teacher()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTeachersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeachersRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['avatar'] = $this->processImage($file, $crop);
            }
        }
        Teacher::create($input);

        return redirect()->route('teachers.index')->with('message',
            trans('messages.create_successful', ['entity' => trans('entities.teacher')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('admin.teachers.edit', [
            'teacher' => $teacher
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTeachersRequest $request
     * @param  \App\Models\Teacher $teacher
     * @return mixed|\Illuminate\Http\Response
     */
    public function update(UpdateTeachersRequest $request, Teacher $teacher)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }

        if ($request->has('crop_data')) {
            if (empty($file)) {
                $url = public_path($teacher->avatar->url());
                if (Storage::exists($url)) {
                    $file = $url;
                }
            }
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['avatar'] = $this->processImage($file, $crop);
            }
        }

        $teacher->update($input);

        if ($request->ajax()) {
            return $teacher;
        }

        return redirect()->route('teachers.index')->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.teacher')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher $teacher
     * @return mixed|\Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        if ($teacher->delete()) {
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.teacher')])];
        }
    }
}
