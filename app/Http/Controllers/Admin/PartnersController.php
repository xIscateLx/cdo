<?php

namespace App\Http\Controllers\Admin;

use Storage;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePartnersRequest;
use App\Http\Requests\UpdatePartnersRequest;
use App\Models\Partner;
use App\Traits\ProcessImageTrait;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    use ProcessImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partners.index', [
            'partners' => Partner::paginate(25),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.edit', [
            'partner' => new Partner()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePartnersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePartnersRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['brand'] = $this->processImage($file, $crop);
            }
        }
        Partner::create($input);

        return redirect()->route('partners.index')->with('message',
            trans('messages.create_successful', ['entity' => trans('entities.partner')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('admin.partners.edit', [
            'partner' => $partner
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePartnersRequest $request
     * @param  \App\Models\Partner $partner
     * @return mixed|\Illuminate\Http\Response
     */
    public function update(UpdatePartnersRequest $request, Partner $partner)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            if (empty($file)) {
                $url = public_path($partner->brand->url());
                if (Storage::exists($url)) {
                    $file = $url;
                }
            }
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['brand'] = $this->processImage($file, $crop);
            }
        }

        $partner->update($input);

        if ($request->ajax()) {
            return $partner;
        }

        return redirect()->route('partners.index')->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.partner')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partner $partner
     * @return mixed|\Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        if ($partner->delete()) {
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.partner')])];
        }
    }
}
