<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateFeedbackRequest;
use Storage;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFeedbackRequest;
use App\Models\Feedback;
use App\Traits\ProcessImageTrait;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    use ProcessImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.feedback.index', ['feedbacks' => Feedback::paginate(25)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.feedback.edit', ['feedback' => new Feedback()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFeedbackRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFeedbackRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['avatar'] = $this->processImage($file, $crop);
            }
        }
        Feedback::create($input);

        return redirect()->route('feedback.index')->with('message',
            trans('messages.create_successful', ['entity' => trans('entities.feedback')]));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        return view('admin.feedback.edit', ['feedback' => $feedback]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateFeedbackRequest $request
     * @param Feedback $feedback
     * @return mixed|\Illuminate\Http\Response
     */
    public function update(UpdateFeedbackRequest $request, Feedback $feedback)
    {
        $input = $request->all();
        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            if (empty($file)) {
                $url = public_path($feedback->avatar->url());
                if (Storage::exists($url)) {
                    $file = $url;
                }
            }
            $crop = json_decode($request->input('crop_data'), true);

            if (!empty($file)) {
                $input['avatar'] = $this->processImage($file, $crop);
            }
        }
        $feedback->update($input);

        if ($request->ajax()) {
            return $feedback;
        }

        return redirect()->route('feedback.index')->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.feedback')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Feedback $feedback
     * @return mixed|\Illuminate\Http\Response
     */
    public function destroy($feedback)
    {
        if ($feedback->delete()) {
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.feedback')])];
        }
    }
}
