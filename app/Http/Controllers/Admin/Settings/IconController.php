<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreIconRequest;
use App\Jobs\UpdateIcons;
use App\Models\Icon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return Icon::all();
        }
        
        return view('admin.settings.icons.index', [
            'icons' => Icon::paginate(25),
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.icons.edit', [
            'icon' => new Icon(),
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreIconRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIconRequest $request)
    {
        $icon = Icon::create($request->all());
        if ($request->hasFile('svg')) {
            $file = $request->file('svg');
            $file_name = $request->input('machine_name') . '.' . $file->extension();
            
            $file->storeAs('images/icons', $file_name);
            
            $icon->update(['file_name' => $file_name]);
        }
        
        $job = (new UpdateIcons($icon))
            ->delay(Carbon::now()->addMinutes(1));
        
        dispatch($job);
        
        return redirect()->route('settings.icons.index')->with('message',
            trans('messages.create_successful', ['entity' => trans('entities.icon')]));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  Icon $icon
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Icon $icon)
    {
        return view('admin.settings.icons.edit', [
            'icon' => $icon,
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Icon                     $icon
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icon $icon)
    {
        if ($request->hasFile('svg')) {
            $file = $request->file('svg');
            
            $file->storeAs('images/icons', $icon->file_name);
            $icon->update(['processed' => false]);
            $job = (new UpdateIcons($icon))
                ->delay(Carbon::now()->addMinutes(1));
            
            dispatch($job);
        }
        $icon->update($request->all());
        
        return redirect()->route('settings.icons.index')->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.icon')]));
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  Icon $icon
     *
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Icon $icon)
    {
        $file_name = 'images/icons' . $icon->file_name;
        if ($icon->delete()) {
            Storage::delete($file_name);
            
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.icon')])];
        }
    }
}
