<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDefaultSettingsRequest;
use App\Models\Variable;
use App\Traits\ProcessImageTrait;
use Illuminate\Support\Facades\Storage;

class DefaultSettingsController extends Controller
{
    use ProcessImageTrait;
    /**
     * @var string
     */
    protected $aboutImage = 'images/default/about.jpg';
    /**
     * @var string
     */
    protected $logoPath = 'images/default/';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('admin.settings.default.edit', [
            'variables' => Variable::all()->pluck('value', 'key')->toArray(),
        ]);
    }

    /**
     * Update default settings
     *
     * @param StoreDefaultSettingsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreDefaultSettingsRequest $request)
    {
        $input = $request->except(['image_file', 'crop_data', '_method', '_token', 'label']);

        foreach ($input as $key => $value) {
            Variable::firstOrNew(['key' => $key])->fill(['value' => $value ?? ''])->save();
        }

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            if (empty($file) && Storage::exists($this->aboutImage)) {
                $file = Storage::get($this->aboutImage);
            }
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $aboutImage = $this->processImage($file, $crop, 'jpg');
                Storage::put($this->aboutImage, $aboutImage->__toString());
                Variable::firstOrNew(['key' => 'about_image'])->fill(['value' => $this->aboutImage])->save();
            }
        }

        if ($request->hasFile('label')) {
            $label = $request->file('label');
            $logoSm = \Image::make($label)->resize(60, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('png');
            $file_name = 'logo-primary.'.$label->extension();
            $label->storeAs($this->logoPath, $file_name);
            Variable::firstOrNew(['key' => 'label'])->fill(['value' => $this->logoPath.$file_name])->save();
            Storage::put($this->logoPath.'logo.png', $logoSm->__toString());
        }

        return back()->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.default_settings')]));;

    }

    /**
     * Delete about image
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function aboutImageDestroy()
    {
        if ($variable = Variable::where('key', 'about_image')->first()) {
            if (Storage::exists($variable->value)) {
                Storage::delete($variable->value);
            }
            $variable->delete();
        }

        return [
            'message' => trans('messages.delete_successful', ['entity' => trans('entities.about_image')]),
            'redirect' => route('settings.default.edit'),
        ];
    }
}
