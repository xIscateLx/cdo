<?php
/**
 * Created by PhpStorm.
 * User: rostislav
 * Date: 06.03.17
 * Time: 12:12
 */

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function edit()
    {
        return view('admin.settings.user.edit');
    }

    public function update(UpdateUserRequest $request)
    {
        $password = $request->input('password');
        $user = \Auth::user();
        $user->forceFill([
            'password'       => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        \Auth::guard()->login($user);

        return back()->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.user')]));
    }
}