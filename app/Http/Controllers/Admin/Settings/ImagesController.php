<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreImagesRequest;
use App\Http\Requests\UpdateImagesRequest;
use App\Models\Image;
use App\Traits\ProcessImageTrait;
use Illuminate\Http\Request;
use Storage;

class ImagesController extends Controller
{
    use ProcessImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings.images.index', [
            'images' => Image::paginate(25),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.images.edit', [
            'image' => new Image()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreImagesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreImagesRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['image'] = $this->processImage($file, $crop);
            }
        }
        Image::create($input);

        return redirect()->route('settings.images.index')->with('message',
            trans('messages.create_successful', ['entity' => trans('entities.image')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        return view('admin.settings.images.edit', [
            'image' => $image
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateImagesRequest $request
     * @param  \App\Models\Image $image
     * @return mixed|\Illuminate\Http\Response
     */
    public function update(UpdateImagesRequest $request, Image $image)
    {
        $input = $request->all();

        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
        }
        if ($request->has('crop_data')) {
            if (empty($file)) {
                $url = public_path($image->image->url());
                if (Storage::exists($url)) {
                    $file = $url;
                }
            }
            $crop = json_decode($request->input('crop_data'), true);
            if (!empty($file)) {
                $input['image'] = $this->processImage($file, $crop);
            }
        }

        $image->update($input);

        if ($request->ajax()) {
            return $image;
        }

        return redirect()->route('settings.images.index')->with('message',
            trans('messages.edit_successful', ['entity' => trans('entities.image')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image $image
     * @return mixed|\Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        if ($image->delete()) {
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.image')])];
        }
    }
}
