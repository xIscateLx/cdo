<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProgramRequest;
use App\Models\Program;
use App\Models\ProgramItem;
use Illuminate\Http\Request;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return Program::with('items', 'items.icon')->get();
        }
        return view('admin.programs.index', [
            'programs' => Program::paginate(25),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.programs.edit', [
            'program' => new Program()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProgramRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProgramRequest $request)
    {
        $program = Program::create($request->all());

        if ($request->has('items')) {
            foreach ($request->input('items') as $item) {
                $program->items()->create($item);
            }
        }

        return response()->json([
            'redirect' => route('programs.index'),
        ]);
    }

    /**
     * @param Request $request
     * @param Program $program
     * @return $this
     */
    public function show(Request $request, Program $program)
    {
        if ($request->ajax()) {
            return $program->load('icon', 'items', 'items.icon');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Program $program
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        return view('admin.programs.edit', ['program' => $program]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreProgramRequest $request
     * @param  Program $program
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProgramRequest $request, Program $program)
    {
        $program->update($request->all());

        $programItems = array_keys($program->items->keyBy('id')->toArray());
        if ($request->has('items')) {
            foreach ($request->input('items') as $item) {
                $item['program_id'] = $program->id;
                if (isset($item['id'])) {
                    $programItem = ProgramItem::firstOrNew(['id' => $item['id']]);
                    $programItem->fill($item);
                    $programItem->save();
                    $programItemKey = array_search($programItem->id, $programItems);
                    if ($programItemKey !== null) {
                        unset($programItems[$programItemKey]);
                    }
                } else {
                    ProgramItem::create($item);
                }

            }
        }
        ProgramItem::destroy($programItems);

        return response()->json([
            'redirect' => route('programs.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Program $program
     *
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        if ($program->delete()) {
            return ['message' => trans('messages.delete_successful', ['entity' => trans('entities.program')])];
        }
    }

    /**
     * Update status on resources in storage
     *
     * @param Request $request
     * @param Program $program
     */
    public function updateStatus(Request $request, Program $program)
    {
        if ($request->ajax() && $request->has('active')) {
            $program->update(['active' => $request->input('active')]);
        }
    }
}
