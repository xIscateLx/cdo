<?php

namespace App\Models;

use App\Traits\ActiveAttributeTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Image
 *
 * @property-write mixed $active
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $image_file_name
 * @property int $image_file_size
 * @property string $image_content_type
 * @property string $image_updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereImageContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereImageFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereImageFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereImageUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereUpdatedAt($value)
 */
class Image extends Model implements StaplerableInterface
{
    use EloquentTrait, ActiveAttributeTrait;
    /**
     * @var array
     */
    protected $fillable = [
        'active',
        'image'
    ];

    /**
     * Image constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'thumb' => '200',
                'pub' => '980x490',
            ],
        ]);
        parent::__construct($attributes);
    }
}
