<?php

namespace App\Models;

use App\Traits\ActiveAttributeTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Partner
 *
 * @property-write mixed $active
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $link
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $brand_file_name
 * @property int $brand_file_size
 * @property string $brand_content_type
 * @property string $brand_updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereBrandContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereBrandFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereBrandFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereBrandUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner whereUpdatedAt($value)
 */
class Partner extends Model implements StaplerableInterface
{
    use EloquentTrait, ActiveAttributeTrait;
    protected $fillable = [
        'name',
        'link',
        'active',
        'brand'
    ];

    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('brand', [
            'styles' => [
                'thumb' => '100',
                'brand' => '200x90',
            ],
        ]);
        parent::__construct($attributes);
    }
}
