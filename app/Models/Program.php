<?php

namespace App\Models;

use App\Traits\ActiveAttributeTrait;
use App\Traits\IconAttributeTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Program
 *
 * @property \App\Models\Icon $icon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramItem[] $items
 * @property-write mixed $active
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $icon_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereIconId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Program whereUpdatedAt($value)
 */
class Program extends Model
{
    use ActiveAttributeTrait, IconAttributeTrait;
    protected $fillable = [
        'name',
        'icon',
        'active',
    ];
    
    public function items()
    {
        return $this->hasMany('App\Models\ProgramItem', 'program_id');
    }
    
    public function icon()
    {
        return $this->belongsTo('App\Models\Icon', 'icon_id');
    }
}
