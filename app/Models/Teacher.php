<?php

namespace App\Models;

use App\Traits\ActiveAttributeTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Teacher
 *
 * @property-write mixed $active
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $about
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $avatar_file_name
 * @property int $avatar_file_size
 * @property string $avatar_content_type
 * @property string $avatar_updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereAbout($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereAvatarContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereAvatarFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereAvatarFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereAvatarUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Teacher whereUpdatedAt($value)
 */
class Teacher extends Model implements StaplerableInterface
{
    use EloquentTrait, ActiveAttributeTrait;
    protected $fillable = [
        'name',
        'position',
        'about',
        'active',
        'avatar'
    ];

    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'thumb' => '50x50',
                'avatar' => '150x150',
            ],
        ]);
        parent::__construct($attributes);
    }
}
