<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Variable
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Variable whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Variable whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Variable whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Variable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Variable whereValue($value)
 */
class Variable extends Model
{
    /**
     * @inherit
     */
    public $fillable = [
        'key',
        'value',
    ];
}
