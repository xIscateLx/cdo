<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Icon
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramItem[] $programItems
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Program[] $programs
 * @property-write mixed $machine_name
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $file_name
 * @property bool $processed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereMachineName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereProcessed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icon whereUpdatedAt($value)
 */
class Icon extends Model
{
    protected $fillable = [
        'name',
        'machine_name',
        'file_name',
        'processed',
    ];

    public function programs()
    {
        return $this->hasMany('App\Models\Program', 'icon_id');
    }

    public function programItems()
    {
        return $this->hasMany('App\Models\ProgramItem', 'icon_id');
    }

    public function setMachineNameAttribute($value)
    {
        $this->attributes['machine_name'] = '#icon-' . $value;
    }
}
