<?php

namespace App\Models;

use App\Traits\IconAttributeTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProgramItem
 *
 * @property \App\Models\Icon $icon
 * @property-read \App\Models\Program $program
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $about
 * @property int $program_id
 * @property int $icon_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereAbout($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereIconId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereProgramId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramItem whereUpdatedAt($value)
 */
class ProgramItem extends Model
{
    use IconAttributeTrait;
    protected $fillable = [
        'name',
        'about',
        'program_id',
        'icon',
    ];
    
    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'program_id');
    }
    
    public function icon()
    {
        return $this->belongsTo('App\Models\Icon', 'icon_id');
    }
    
    
}
