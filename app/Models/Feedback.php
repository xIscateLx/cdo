<?php

namespace App\Models;

use App\Traits\ActiveAttributeTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Feedback
 *
 * @property-write mixed $active
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $avatar_file_name
 * @property int $avatar_file_size
 * @property string $avatar_content_type
 * @property string $avatar_updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereAvatarContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereAvatarFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereAvatarFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereAvatarUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feedback whereUpdatedAt($value)
 */
class Feedback extends Model implements StaplerableInterface
{
    use EloquentTrait, ActiveAttributeTrait;
    protected $fillable = [
        'name',
        'position',
        'message',
        'active',
        'avatar'
    ];

    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'thumb' => '50x50',
                'avatar' => '150x150',
            ],
        ]);
        parent::__construct($attributes);
    }
}
