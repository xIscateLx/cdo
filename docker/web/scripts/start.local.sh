#!/usr/bin/env bash

npm install
npm install --global gulp-cli bower

bower install --quiet --allow-root

gulp watch
