#!/usr/bin/env bash

docker/wait-for-it.sh database:3306 --timeout=120

php /usr/local/bin/composer.phar install

php artisan migrate --force

nginx

php-fpm --allow-to-run-as-root