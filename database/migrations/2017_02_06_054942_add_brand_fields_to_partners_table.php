<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBrandFieldsToPartnersTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function(Blueprint $table) {

            $table->string('brand_file_name')->nullable();
            $table->integer('brand_file_size')->nullable()->after('brand_file_name');
            $table->string('brand_content_type')->nullable()->after('brand_file_size');
            $table->timestamp('brand_updated_at')->nullable()->after('brand_content_type');

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function(Blueprint $table) {

            $table->dropColumn('brand_file_name');
            $table->dropColumn('brand_file_size');
            $table->dropColumn('brand_content_type');
            $table->dropColumn('brand_updated_at');

        });
    }

}