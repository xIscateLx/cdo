<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('about')->nullable();
            $table->integer('program_id')->nullable()->unsigned();
            $table->integer('icon_id')->nullable()->unsigned();
            $table->timestamps();
    
            $table->foreign('program_id')
                ->references('id')->on('programs')
                ->onDelete('cascade');
    
            $table->foreign('icon_id')
                ->references('id')->on('icons')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_items');
    }
}
